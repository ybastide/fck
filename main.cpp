/* 
 * File:   main.cpp
 * Author: zeb
 *
 * Created on 13 juillet 2014, 17:59
 */

#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <unordered_map>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/syscall.h>

#include "Control.h"


using namespace std;

namespace {

#if 1
#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)
#else
#define likely(x)      x
#define unlikely(x)    x
#endif
char const *progname;

unordered_map<pid_t, Control> ctrls;

// 5 derniers pids (5 * 8 + 5 * 4 + 4 = 64 octets)
namespace Lasts {
	Control * pc_[5];
	pid_t pid_[5];
	int last_over_;

//public:

	Control *get_pc(pid_t p)
	{
#define GETRET(n) if (p == pid_[n]) return pc_[n]
		GETRET(0);
		GETRET(1);
		GETRET(2);
		GETRET(3);
		GETRET(4);
		return nullptr;
#undef GETRET
	}

	void set_pc(pid_t p, Control *pc)
	{
#define SETN(n) {pid_[n]=p;pc_[n]=pc;}
#define SETIFN(n) if (!pid_[n]) SETN(n)
		SETIFN(0)
		else SETIFN(1)
		else SETIFN(2)
		else SETIFN(3)
		else SETIFN(4)
		else {
			SETN(last_over_);
			last_over_ = (last_over_ + 1) % 5;
		}
#undef SETN
	}

	void erase(pid_t p)
	{
#define DELIF(n) if (p == pid_[n]) pid_[n]=0
		DELIF(0);
		DELIF(1);
		DELIF(2);
		DELIF(3);
		DELIF(4);
#undef DELIF
	}
};

//Lasts lasts;

pid_t child_pid;
int exit_code;
FILE *out = stderr;

void myprintf(char const *s)
{
	fputs(s, out);
}

void myprintf(const char *format, va_list ap)
{
	vfprintf(out, format, ap);
}

void myprintf(char const *format, ...)
{
	va_list ap;
	va_start(ap, format);
	myprintf(format, ap);
	va_end(ap);
}

void die(void) __attribute__((noreturn));

void die(char const *s)
{
	fprintf(stderr, "%s: %s\n", progname, s);
	exit(1);
}

void die_error(void) __attribute__((noreturn));

void die_error(char const *s)
{
	fprintf(stderr, "%s: %s: %s\n", progname, s, strerror(errno));
	exit(1);
}

void execute(char** argv, char **envp)
{
	auto filename = argv[0];

	auto pid = fork();
	if (pid < 0) {
		die_error("fork");
	}
	// TODO fermer fichiers ouverts éventuels
	if (!pid) {
		// child
		if (ptrace(PTRACE_TRACEME, 0L, nullptr, nullptr) < 0) {
			die_error("ptrace(PTRACE_TRACEME)");
		}
		// sinon execve raise TRAP et fiston meurt
		raise(SIGSTOP);
		//		printf("filename=%s\n", filename);
		//		printf("argv=");
		//		for (int i = 0; i < 10; i++) {
		//			if(!argv[i])
		//				break;
		//			printf(" [%d]:'%s'", i, argv[i]);
		//
		//		}
		//		printf("\n");
		//		printf("envp=");
		//		for (int i = 0; i < 10; i++) {
		//			if(!envp[i])
		//				break;
		//			printf(" [%d]:'%s'", i, envp[i]);
		//
		//		}
		//		printf("\n");
		execve(filename, argv, envp);
		die_error("execve");
	}

	// parent
	child_pid = pid;
	ctrls[pid] = Control(/*pid,*/ true);
}

void setup_signals()
{
	//	sigemptyset(&empty_set);
	//	sigemptyset(&blocked_set);
	struct sigaction sa;
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sigaction(SIGTTOU, &sa, NULL);
	sigaction(SIGTTIN, &sa, NULL);
	sigaction(SIGHUP, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGQUIT, &sa, NULL);
	sigaction(SIGPIPE, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);
}

void trace()
{
	while (!ctrls.empty()) {
		int status;
		// FIXME pour timers (on peut choper l'usage global sur exit()).
		// FIXME Voir si utile, ou si on remet waitpid.
		//struct rusage usage;
		//auto pid = wait4(-1, &status, __WALL, &usage);
		auto pid = waitpid(-1, &status, __WALL);
		if (pid < 0) {
			if (errno == EINTR)
				continue;
			die_error("waitpid");
			//break;
		}
		auto event = ((unsigned) status >> 16);

		Control *pc = Lasts::get_pc(pid);
		if (unlikely(pc == nullptr)) {
			auto cit = ctrls.find(pid);
			if (cit == end(ctrls)) {
				// nouveau fiston
				ctrls[pid] = Control(/*pid,*/ true);
				pc = &ctrls[pid];
				myprintf("New Process: %d\n", pid);
			} else {
				pc = &cit->second;
				myprintf("pid=%u not in lasts\n", pid);
			}
			Lasts::set_pc(pid, pc);
		}

		if (unlikely(event == PTRACE_EVENT_EXEC)) {
			// execve
			long old_pid;
			if (ptrace(PTRACE_GETEVENTMSG, pid, NULL, (long) &old_pid) < 0)
				die_error("ptrace(PTRACE_GETEVENTMSG)");
			if (old_pid < 0) {
				die("old_pid < 0");
			}
			myprintf("pid %u: execve (from pid %u)\n", pid, (pid_t) old_pid);
			if (old_pid != pid) {
				// oublie le thread qui a fait execve
				ctrls.erase(old_pid);
				Lasts::erase(old_pid);
			}
		}

		if (unlikely(WIFSIGNALED(status))) {
			if (pid == child_pid) {
				exit_code = 0x100 | WTERMSIG(status);
			}
			myprintf("pid %u: +++ killed by %s (status 0x%x) %s+++\n",
				pid,
				strsignal(WTERMSIG(status)),
				status,
				WCOREDUMP(status) ? "(core dumped) " : "");
			ctrls.erase(pid);
			Lasts::erase(pid);
		} else if (unlikely(WIFEXITED(status))) {
			if (pid == child_pid) {
				exit_code = WEXITSTATUS(status);
			}
			myprintf("pid %u: exited, code %d\n",
				pid,
				WEXITSTATUS(status));
			ctrls.erase(pid);
			Lasts::erase(pid);
		} else if (unlikely(!WIFSTOPPED(status))) {
			// impossible, non ?
			myprintf("PANIC: pid %u not stopped\n", pid);
			ctrls.erase(pid);
			Lasts::erase(pid);
		} else {
			if (unlikely(pc->just_started())) {
				myprintf("pid %u: starting trace\n", pid);
				pc->reset_just_started();
				if (ptrace(PTRACE_SETOPTIONS, pid, nullptr, (long) (PTRACE_O_TRACECLONE | PTRACE_O_TRACEEXEC | PTRACE_O_TRACEFORK | PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACEVFORK)) < 0) {
					if (errno != ESRCH) {
						die_error("ptrace(PTRACE_SETOPTIONS)");
					}
				}
			}

			auto sig = WSTOPSIG(status);

			if (likely(sig == SIGTRAP | 0x80)) {
				// syscall
				sig = 0;
			} else if (unlikely(event != 0)) {
				sig = 0;
				myprintf("pid %u: event %u\n", pid, event);
			}

			// Continuer
			if (unlikely(sig))
				myprintf("pid %u: PTRACE_SYSCALL, sig=%s (%d)\n", pid, strsignal(sig), sig);
			if (unlikely(ptrace(PTRACE_SYSCALL, pid, nullptr, sig) < 0)) {
				if (errno != ESRCH) {
					die_error("ptrace(PTRACE_SYSCALL)");
				}

			}
		}

	}
}

} // namespace

/*
 * 
 */
int main(int argc, char** argv, char **envp)
{
	progname = argv[0];
	if (argc < 2) {
		fprintf(stderr, "Usage: %s app-path app-args\n", progname);
		return 1;
	}
	execute(argv + 1, envp);
	// après fork+exec sinon fiston en hérite
	setup_signals();
	trace();
	return 0;
}

