/* 
 * File:   Control.cpp
 * Author: zeb
 * 
 * Created on 13 juillet 2014, 18:00
 */

#include "Control.h"

Control::Control() {
	//pid_ = -1;
	started_ = false;
}

Control::Control(/*pid_t pid,*/ bool started) {
	//pid_ = pid;
	started_ = started;
}

Control::Control(const Control& orig)
	:
	//pid_(orig.pid_),
	started_(orig.started_)
{
}

void Control::reset_just_started()
{
	started_ = false;
}

Control::~Control() {
}

