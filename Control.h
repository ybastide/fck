/* 
 * File:   Control.h
 * Author: zeb
 *
 * Created on 13 juillet 2014, 18:00
 */

#ifndef CONTROL_H
#define	CONTROL_H

#include <sys/types.h>
#include <unistd.h>

class Control {
public:
    Control();
    explicit Control(/*pid_t pid,*/ bool started);
    Control(const Control& orig);
    /*virtual*/ ~Control();
    
//    pid_t get_pid() const
//    {
//        return pid_;
//    }
    
    void reset_just_started();
    bool just_started() const
    {
        return started_;
    }
private:
//    pid_t pid_;
    bool started_;
};

#endif	/* CONTROL_H */

